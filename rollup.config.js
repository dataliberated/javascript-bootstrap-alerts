import resolve from "rollup-plugin-node-resolve"
import commonjs from 'rollup-plugin-commonjs';
import { string } from "rollup-plugin-string";
import { terser } from "rollup-plugin-terser";

export default {
	input: "src/js/main.js",
	output: {
		file: "dist/javascript-bootstrap-alerts.min.js",
		format: "iife",
		name: "JavascriptBootstrapAlerts"
	},
	plugins: [
		resolve({
			browser: true
		}),
		commonjs(),
		string({
			include: "src/html/*.html"
		}),
		terser({
			output: {
				comments: function(){}
			}
		})
	]
}
