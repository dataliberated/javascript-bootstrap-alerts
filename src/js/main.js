import mustache from "mustache";
import alertTemplate from "../html/alert.html";

function addAltertFromMarkup(str) {
	var wrapper = document.createElement("div");
	wrapper.innerHTML = str;
	document.getElementsByClassName("alert-container")[0]
		.appendChild(wrapper.firstChild);
}

export function renderSuccess(msg) {
	var variables = {
		type: "success",
		symbol: "fe-check",
		message: msg
	};
	var htmlStr = mustache.render(alertTemplate, variables);
	addAltertFromMarkup(htmlStr);
}

export function renderDanger(msg) {
	var variables = {
		type: "danger",
		symbol: "fe-alert-triangle",
		message: msg
	};
	var htmlStr = mustache.render(alertTemplate, variables);
	addAltertFromMarkup(htmlStr);
}

export function renderCustom(t, s, msg) {
	var variables = {
		type: t,
		symbol: s,
		message: msg
	};
	var htmlStr = mustache.render(alertTemplate, variables);
	addAltertFromMarkup(htmlStr);
}
